
/**
 * Returns the sum of the range between number 1 and number 2.
 * @param  {number} number1
 * @param  {number} number2
 * @returns {number} total
 */
let sum = (num1, num2)=>{
    if (num1 < 0 || num2 < 0){
        return {
            error: true,
            message: 'Los números tienen que ser positivos',
        };
    }
    if (!Number.isInteger(num1) || !Number.isInteger(num2)) {
        return {
            error: true,
            message: 'Los números tienen que ser enteros',
        };
    }
    if (num1 > num2) {
        return {
            error: true,
            message: 'El número 1 tiene que ser mayor al número 2',
        };
    }
    let total = 0;
    for (let i = num1; i <= num2; i++) {
        total += i;
    }
    return {
        error: false,
        message: `La suma total del rango es: ${total}`,
    };
}

module.exports = {
    sum
}