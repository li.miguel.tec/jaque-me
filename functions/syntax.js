function validSintax(value){
    value = value.replace(/\(\)/gi, '');
    value = value.replace(/\[\]/gi, '');
    if ( value.includes('()') || value.includes('[]') ){
        return validSintax(value);
    }
    return (value.length==0);
}


/**
 * Returns if the syntax is correct or not.
 * @param  {string} value
 * @returns {Object} response
 */
let syntax = (value)=>{
    if ( value===undefined ){
        return {
            error: true,
            message: 'La cadena a evaluar no puede estar vacía',
        };
    }
    value = value.replace(/ /gi, '');
    if ( value.length < 1 ){
        return {
            error: true,
            message: 'La cadena a evaluar no puede estar vacía',
        };
    }
    return {
        error: false,
        message: validSintax( value ) ? 'La sintaxis de la cadena es correcta' : 'La sintaxis de la cadena es incorrecta',
    };
}

module.exports = {
    syntax
}