
/**
 * Returns the value of determined position.
 * @param  {Object[]} numbers
 * @param  {number} position
 * @returns {Object} response
 */
let bigger = (numbers, position)=>{
    if( !Array.isArray(numbers) ){
        return {
            error: true,
            message: 'El valor tiene que ser un arreglo'
        };
    }
    if (!Number.isInteger(position)){
        return {
            error: true,
            message: 'La posición deseada tiene que ser un número entero'
        };
    }
    if (position < 1){
        return {
            error: true,
            message: 'Tiene que ingresar una posición mayor a cero'
        };
    }
    if (position > numbers.length){
        return {
            error: true,
            message: `La posición (${position}) no puede ser mayor a la cantidad de elementos (${numbers.length})`
        };
    }
    const sortNumbers = numbers.sort( (a,b)=>b-a );
    return {
        error: false,
        message: `El valor solicitado es: ${sortNumbers[position - 1]}`
    };
}

module.exports = {
    bigger
}