function getSubArrays(numbers){
    let arrays = [];
    let auxArray = [];
    for (let i = 0; i < numbers.length; i++) {
        if ( auxArray.length==0 || numbers[i - 1] < numbers[i] ){
            auxArray.push(numbers[i]);
        }
        if ( numbers[i] >= numbers[i + 1] ){
            arrays.push( auxArray );
            auxArray = [];
        }
    }
    if ( auxArray.length > 0 ){
        arrays.push( auxArray );
    }
    return arrays;
}


/**
 * Returns an ascending subarray with max length.
 * @param  {Object[]} numbers
 * @returns {Object} response
 */
let subarrays = (numbers)=>{
    if ( !Array.isArray(numbers) ){
        return {
            error: true,
            message: 'El valor tiene que ser un arreglo'
        };
    }
    if ( numbers.length < 1 ){
        return {
            error: true,
            message: 'El arreglo tiene que tener al menos un elemento'
        };
    }

    let arrays = getSubArrays(numbers);

    let index = 0;
    let length = arrays[index].length;
    for (let i = 1; i < arrays.length; i++) {
        if ( arrays[i].length > length ){
            length = arrays[i].length;
            index = i;
        }
    }
    return {
        error: false,
        message: `El subarreglo es: [${arrays[index]}]`
    };
}

module.exports = {
    subarrays
}