function formatter(classes){
    let scheduleValidated = [];
    let item;
    for (let i = 0; i < classes.length; i++) {
        classes[i] = classes[i].replace(/ /gi, '');
        item = classes[i].split('-');
        //Valida formato correcto
        if ( item.length!=2 || item[0].split(':').length!=2 || item[1].split(':').length!=2 || item[0].length!=5 || item[1].length!=5){
            return {
                error: true,
                message: 'Las clases tienen que tener el formato hh:mm-hh:mm',
            };
        }

        let classItem = item[0].split(':').concat(item[1].split(':'));
        for (let j = 0; j < classItem.length; j++) {
            classItem[j] = parseInt(classItem[j]);
            //Valida que sean números
            if ( isNaN(classItem[j]) || classItem[j] < 0 ){
                return {
                    error: true,
                    message: 'Las horas y los minutos tienen que ser números válidos',
                };
            }
            //Valida que las horas no sean mayores a 23 y los minutos mayores a 59
            if ( ((j==0 || j==2) && classItem[j]>23) || ((j==1 || j==3) && classItem[j]>59) ){
                return {
                    error: true,
                    message: 'Las horas no puedes ser mayores a 24 y los mínutos mayores a 59',
                };
            }
            if ( (j==1 || j==3) && classItem[j]<10 ){
                classItem[j] = `0${classItem[j]}`;
            }
        }
        if ( parseInt( `${classItem[0]}${classItem[1]}` ) >= parseInt( `${classItem[2]}${classItem[3]}` ) ){
            return {
                error: true,
                message: 'La hora final tiene que ser mayor a la hora inicial',
            };
        }
        scheduleValidated.push([parseInt( `${classItem[0]}${classItem[1]}` ), parseInt( `${classItem[2]}${classItem[3]}` )]);
    }
    return {
        error: false,
        array: scheduleValidated,
    }
}
function getNumClassrooms(classes){
    let classRooms = [];
    let item1, item2 = [];
    let added = false;
    classes = classes.sort( (a,b)=>a[0]-b[0] );

    classRooms.push( classes[0] );
    for (let i = 1; i < classes.length; i++) {
        item1 = classes[i];
        added = false;
        for (let j = 0; j < classRooms.length; j++) {
            item2 = classRooms[j];
            if ( item1[0] > item2[ item2.length - 1 ] ){
                item2 = item2.concat(item1);
                classRooms[j] = classRooms[j].concat(item1);
                added = true;
                break;
            }
        }
        if ( added==false ){
            classRooms.push( item1 );
        }
    }
    return classRooms;
}


/**
 * Returns the minimum number of classrooms.
 * @param  {Object[]} rangeTime
 * @returns {Object} response
 */
let schedule = (classes)=>{
    if( !Array.isArray(classes) ){
        return {
            error: true,
            message: 'El valor tiene que ser un arreglo'
        };
    }
    if( classes.length < 1 ){
        return {
            error: true,
            message: 'El arreglo tiene que tener al menos un valor'
        };
    }

    let response = formatter(classes);
    if ( response.error==true ){
        return response;
    }

    let numClassrooms = getNumClassrooms( response.array );
    return {
        error: false,
        message: `El número mínimo de salones es: ${numClassrooms.length}`
    };

}

module.exports = {
    schedule
}