
/**
 * Returns an array without duplicates values.
 * @param  {Object[]} numbers
 * @returns {Object} response
 */
let duplicates = (numbers)=>{
    if( !Array.isArray(numbers) ){
        return {
            error: true,
            message: 'El valor tiene que ser un arreglo'
        };
    }
    return {
        error: false,
        message: `El arreglo sin duplicados es: [${[...new Set( numbers )]}]`,
    };
}

module.exports = {
    duplicates
}