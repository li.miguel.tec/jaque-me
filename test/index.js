'use strict';

let firebase = require('./db');
const globals = require('./globals');

init();
async function init(){
    await firebase.initFirebase();

    console.info( globals.firebase );
}