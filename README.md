## Ejercicios BED
### Instalaciones

- npm install

### Probar funciones

*Ayuda*

- node app help

*Ejercicio 1*

- node app subarrays --numbers a b c d e f g

```
node app subarrays --numbers 3 2 5 9 1 3
node app subarrays -x 3 2 5 9 1 3
```

*Ejercicio 2*

- node app sum --num1 x --num2 y

```
node app sum --num1 3 --num2 6
node app sum -a 3 -b 6
```

*Ejercicio 3*

- node app bigger --numbers a b c d e f g --position x

```
node app bigger --numbers 7 2 5 9 8 3 --position 2
node app bigger -x 7 2 5 9 8 3 -p 2
```

*Ejercicio 4*

- node app duplicates --numbers a b c d e f g

```
node app duplicates --numbers 7 5 5 9 4 3 3 5
node app duplicates -x 7 5 5 9 4 3 3 5
```

*Ejercicio 5*

- node app syntax --value "cadena"

```
node app syntax --value '(()]'
node app syntax --value '[()]'
```

*Ejercicio 6*

- node app schedule --values a b c d e f g

```
node app schedule --values '11:00-14:00' '10:00-10:30' '08:00-23:00' '12:00-15:00' '14:30-16:00' '16:00-18:00'
node app schedule --v '11:00-14:00' '10:00-10:30' '08:00-23:00' '12:00-15:00' '14:30-16:00' '16:00-18:00'
```


### Ejecución de pruebas
```
npm run test
```


### Análisis

*Algunas validaciones pudieron hacerse más rápido con alguna librería como AJV y el ejercicio de los horarios con moment, pero no use ningún paquete porque no sabía si realmente se podían usar*

*1. Escribir una función que reciba un arreglo de números y obtenga el subarreglo más grande en el que cada elemento del subarreglo sea mayor que el anterior. Por ejemplo si el arreglo es [3,2,5,9,1,3] tiene regresar [2,5,9].*

```
Análisis: Mi idea fue dividir el arreglo principal en subarreglos que cumplan con la condición de que deben de estar en orden ascendente. Al final muestro el elemento que tiene el mayor número de elementos.
Complejidad: 3/5
```

-------


*2. Escribir una función que reciba 2 enteros no negativos n y m y obtenga el resultado de n+(n+1)+(n+2)+...+m​. Por ejemplo si los numeros son 3 y 6 la respuesta debe ser 18 que es el resultado de 3+4+5+6.*

```
Análisis: Primero asegurarse que los números sean válidos. después de eso es solamente una suma en el rango de los números.
Complejidad: 2/5
```
-------


*3. Dado un arreglo de números desordenado y un entero k, escribe una función que encuentre el k-ésimo elemento más grande. Por ejemplo si el arreglo es [7,2,5,9,8,3] y k es 2 debe regresar 8.*

```
Análisis: Primero ordenar el arreglo y devolver la posición solicitada.
Complejidad: 2/5
```
-------


*4. Escribe una función que reciba un arreglo de números y quite los elementos duplicados.*

```
Análisis: Cuando se conocen ciertas propiedades es muy fácil. En este caso los Set no permiten valores duplicados, entonces solamente lo pase a un Set y del Set a un arreglo.
Complejidad: 2/5
```
-------


*5. Dada una cadena de paréntesis y corchetes escribe una función que regresa si la cadena está bien balanceada, es decir, por cada paréntesis o corchete que abre hay uno al mismo nivel que que cierra. Por ejemplo si recibe ‘([])’ o ‘[()[]]’ tiene que regresar true y si recibe ‘([)]’ tiene que regresar false.*

```
Análisis: Inicialmente probé con una expresión regular y estoy seguro que se puede resolver con una, pero después de varios intentos fallidos decidí hacerlo con una función recursiva, eliminando en cada llamado los pares de paréntesis y corchetes, si al final no quedan caracteres entonces es valido.
Complejidad: 2/5
```
-------


*6. Tienes un conjunto de clases que tienen una hora de inicio y una de final, hay que acomodarlas en el menor número de salones sin que se traslapan, una clase de 11:00 a 14:00 no puede estar en el mismo salón que una de 12:00 a 15:00. Escribe una función que encuentre ese número de salones, por ejemplo si tienes clases de 11:00 a 14:00, 12:00 a 15:00 y 14:30 a 16:00 el menor número de salones es 2.*

```
Análisis: Lo principal es validar que la información sea correcta, una vez hecho esto ordenar el arreglo para que se vayan agregando subarreglos ordenados que cumplan con la validación de que no se traslapen; al final solamente se devuelve el número de subarreglos generados.
Complejidad: 4/5
```
-------