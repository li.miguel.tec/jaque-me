const opts = {
    base: {
        demand: true,
        alias: 'b'
    },
    limit: {
        alias: 'l',
        default: 10,
    },
};

const argv = require('yargs')
    .command('subarrays', 'Returns an ascending subarray with max length', {
        numbers: {
            demandOptions: true,
            type: 'array',
            alias: 'x'
        }
    })
    .command('sum', 'Retuns the sum of the range beetwen number1 and number 2', {
        num1: {
            demandOption: true,
            alias: 'a',
            type: 'number'
        },
        num2: {
            demandOption: true,
            alias: 'b',
            type: 'number'
        }
    })
    .command('bigger', 'Returns the value of determined position.', {
        numbers: {
            demandOptions: true,
            type: 'array',
            alias: 'x'
        },
        position: {
            demandOption: true,
            alias: 'p',
            type: 'number'
        }
    })
    .command('duplicates', 'Returns an array without duplicates values', {
        numbers: {
            demandOptions: true,
            type: 'array',
            alias: 'x'
        },
    })
    .command('syntax', 'Returns if the syntax is correct or not', {
        value: {
            demandOption: true,
        }
    })
    .command('schedule', 'Returns the minimum number of classrooms', {
        values: {
            demandOption: true,
            type: 'array',
            alias: 'v'
        }
    })
    .help()
    .argv;

module.exports = {
    argv
}