const faker = require('faker');
const { syntax } = require('../functions/syntax');

describe('syntax', ()=> {
    const chars = ['[', '(', ')', ']'];
    
    it('should returns if the syntax is correct or not', done=>{
        let value = '';
        for (let i = 0; i < 4; i++) {
            value += chars[faker.random.number({min:0, max:(chars.length - 1)})];
        }
        console.log( `Cadena generada: ${value}` );
        let resp = syntax(value);
        console.log( resp.message );
        if ( resp['error']==true ) done(resp);
        else done();
    });

    it('should returns error since it has an empty value', done=>{
        let resp = syntax('');
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });
});