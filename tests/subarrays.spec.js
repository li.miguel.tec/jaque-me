const faker = require('faker');
const { subarrays } = require('../functions/subarrays');

describe('subarrays', ()=> {

    it('should returns an ascending subarray with max length', done=>{
        const length = faker.random.number({min:15, max:30});
        const myArray = [];
        for (let i = 0; i < length; i++) {
            myArray.push( faker.random.number({min:1, max:20}) );
        }
        console.log( `Arreglo generado: [${myArray}]` );
        let resp = subarrays(myArray);
        console.log( resp.message );
        if ( resp['error']==true ) done(resp);
        else done();
    });

    it('should returns error since it has an invalid value', done=>{
        const value = faker.lorem.word();
        let resp = subarrays(value);
        console.log( `Valor generado: ${value}` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });

    it('should returns error since it has an empty value', done=>{
        let resp = subarrays([]);
        console.log( `Valor generado: []` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });
});