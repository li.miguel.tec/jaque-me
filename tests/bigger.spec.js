const faker = require('faker');
const { bigger } = require('../functions/bigger');

describe('bigger', ()=> {
    it('should returns an ascending subarray with max length', done=>{

        const length = faker.random.number({min:15, max:30});
        const position = faker.random.number({min:1, max:length});
        const myArray = [];
        for (let i = 0; i < length; i++) {
            myArray.push( faker.random.number({min:1, max:100}) );
        }
        let resp = bigger(myArray, position);
        console.log( `Arreglo generado: [${myArray}]` );
        console.log( `Posición solicitada: ${position}` );
        console.log( resp.message );
        if ( resp['error']==true ) done(resp);
        else done();
    });

    it('should returns error since it has an invalid position', done=>{
        const length = faker.random.number({min:15, max:30});
        const position = faker.random.number({min:(length + 1)});
        const myArray = [];
        for (let i = 0; i < length; i++) {
            myArray.push( faker.random.number({min:1, max:100}) );
        }
        let resp = bigger(myArray, position);
        console.log( `Arreglo generado: [${myArray}]` );
        console.log( `Posición solicitada: ${position}` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });

    it('should returns error since it has an invalid position value', done=>{
        const length = faker.random.number({min:15, max:30});
        const position = faker.lorem.word();
        const myArray = [];
        for (let i = 0; i < length; i++) {
            myArray.push( faker.random.number({min:1, max:100}) );
        }
        let resp = bigger(myArray, position);
        console.log( `Arreglo generado: [${myArray}]` );
        console.log( `Posición solicitada: ${position}` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });
});