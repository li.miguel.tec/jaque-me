const faker = require('faker');
const { duplicates } = require('../functions/duplicates');

describe('duplicates', ()=> {
    it('should returns an array without duplicates values', done=>{

        const length = faker.random.number({min:15, max:30});
        const myArray = [];
        for (let i = 0; i < length; i++) {
            myArray.push( faker.random.number({min:1, max:10}) );
        }
        console.log( `Arreglo generado: [${myArray}]` );
        let resp = duplicates(myArray);
        console.log( resp.message );
        if ( resp['error']==true ) done(resp);
        else done();
    });

    it('should returns error since it has an invalid value', done=>{
        const value = faker.lorem.word();
        let resp = duplicates(value);
        console.log( `Valor generado: ${value}` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });
});