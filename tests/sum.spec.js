const faker = require('faker');
const { sum } = require('../functions/sum');

describe('sum', ()=> {
    it('should returns the sum of the range beetwen number1 and number 2', done=>{
        const num1 = faker.random.number({min:1, max:100});
        const num2 = faker.random.number({min:(num1 + 1), max:101});
        let resp = sum(num1, num2);
        console.log( `Número 1: ${num1}` );
        console.log( `Número 2: ${num2}` );
        console.log( resp.message );
        if ( resp['error']==true ) done(resp);
        else done();
    });

    it('should returns error since it has an invalid number 1', done=>{
        const num1 = faker.lorem.word();
        const num2 = faker.random.number({min:1, max:100});
        let resp = sum(num1, num2);
        console.log( `Número 1: ${num1}` );
        console.log( `Número 2: ${num2}` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });

    it('should returns error since it has an invalid number 2', done=>{
        const num1 = faker.random.number({min:1, max:100});
        const num2 = faker.lorem.word();
        let resp = sum(num1, num2);
        console.log( `Número 1: ${num1}` );
        console.log( `Número 2: ${num2}` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });

    it('should returns error since it has an invalid range', done=>{
        const num2 = faker.random.number({min:1, max:100});
        const num1 = faker.random.number({min:(num2 + 1), max:100});
        let resp = sum(num1, num2);
        console.log( `Número 1: ${num1}` );
        console.log( `Número 2: ${num2}` );
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });
});