const faker = require('faker');
const { schedule } = require('../functions/schedule');

describe('schedule', ()=> {

    it('Returns the minimum number of classrooms', done=>{
        const length = faker.random.number({min:4, max:9});
        const myArray = [];
        let iniHour, finHour, iniMinute, finMinute, item;
        for (let i = 0; i < length; i++) {
            iniHour = faker.random.number({min:0, max:15});
            iniMinute = faker.random.number({min:0, max:59});
            finHour = faker.random.number({min:(iniHour + 1), max:23});
            finMinute = faker.random.number({min:0, max:59});
            item = String(iniHour<10 ? `0${iniHour}` : iniHour) + ':' + String(iniMinute<10 ? `0${iniMinute}` : iniMinute) + '-';
            item += String(finHour<10 ? `0${finHour}` : finHour) + ':' + String(finMinute<10 ? `0${finMinute}` : finMinute);
            myArray.push( item );
        }
        console.log( `Arreglo generado: [${myArray}]` );
        let resp = schedule(myArray);
        console.log( resp.message );
        if ( resp['error']==true ) done(resp);
        else done();
    });

    it('should returns error since it has an invalid format', done=>{
        const length = faker.random.number({min:4, max:9});
        const myArray = [];
        let iniHour, finHour, iniMinute, finMinute, item;
        for (let i = 0; i < length; i++) {
            iniHour = faker.random.number({min:0, max:15});
            iniMinute = faker.random.number({min:0, max:59});
            finHour = faker.random.number({min:(iniHour + 1), max:23});
            finMinute = faker.random.number({min:0, max:59});
            item = String(iniHour<10 ? `0${iniHour}` : iniHour) + '_' + String(iniMinute<10 ? `0${iniMinute}` : iniMinute) + '-';
            item += String(finHour<10 ? `0${finHour}` : finHour) + '_' + String(finMinute<10 ? `0${finMinute}` : finMinute);
            myArray.push( item );
        }

        console.log( `Arreglo generado: [${myArray}]` );
        let resp = schedule(myArray);
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });

    it('should returns error since it has an invalid value', done=>{
        const myArray = faker.random.word();

        console.log( `Arreglo generado: [${myArray}]` );
        let resp = schedule(myArray);
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });

    it('should returns error since it has an empty value', done=>{
        console.log( `Arreglo generado: []` );
        let resp = schedule([]);
        console.log( resp.message );
        if ( resp['error']==true ) done();
        else done(resp.message);
    });
});