const user = new ModelProjectType({
    type: 'residential'.toUpperCase(),
    disabled: false,
    link: 'abc',
    properties: [],
    companies: [],
    workingHours: [
      {
        weekDay: 1,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 2,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 3,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 4,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 5,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 6,
        opensAt: '09:00',
        closesAt: '14:00',
      },
      {
        weekDay: 7,
        opensAt: '00:00',
        closesAt: '00:00',
      },
    ],
  });

  const user = new ModelProjectType({
    link: 'abc',
    type: 'corporate'.toUpperCase(),
    disabled: false,
    properties: [],
    companies: [],
    workingHours: [
      {
        weekDay: 1,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 2,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 3,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 4,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 5,
        opensAt: '09:00',
        closesAt: '18:00',
      },
      {
        weekDay: 6,
        opensAt: '00:00',
        closesAt: '00:00',
      },
      {
        weekDay: 7,
        opensAt: '00:00',
        closesAt: '00:00',
      },
    ],
  });

  