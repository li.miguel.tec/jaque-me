const { argv } = require('./config/yargs');
const colors = require('colors');

const { subarrays } = require('./functions/subarrays');
const { sum } = require('./functions/sum');
const { bigger } = require('./functions/bigger');
const { duplicates } = require('./functions/duplicates');
const { syntax } = require('./functions/syntax');
const { schedule } = require('./functions/schedule');

let command = argv._[0];

// let response = schedule(['11:00-14:00', '10:00-10:30', '08:00-23:00', '12:00-15:00', '14:30-16:00', '16:00-18:00']);
// node app --schedule '11:00-14:00' '10:00-10:30' '08:00-23:00' '12:00-15:00' '14:30-16:00' '16:00-18:00'
// console.log(response);

function showResult(response){
    if ( response['error']==true )
        console.log( (response.message).red );
    else
        console.log( (response.message).green );
}

switch (command) {
    case 'subarrays':
        showResult( subarrays( argv.numbers ) );
        break;
    case 'sum':
        showResult( sum(argv.num1, argv.num2) );
        break;
    case 'bigger':
        showResult( bigger(argv.numbers, argv.position) );
        break;
    case 'duplicates':
        showResult( duplicates(argv.numbers) );
        break;
    case 'syntax':
        showResult( syntax( argv.value ) );
        break;
    case 'schedule':
        showResult( schedule( argv.values ) );
        break;
    default:
        showResult( {error:true, message:'Comando no reconocido'} );
        break;
}

const result = [
    {
        "id": "dsfsdf",
        "label": "hsajdhskaj",
        "ptoyectType": "jhsajdhsajkdhsa",
        "active": "dsljhfdsjfk",
        "updateDate": "kjhsdkjhsdk"
    },
    {
        "id": "34543",
        "label": "hsajdhskaj",
        "ptoyectType": "34543",
        "active": "34543",
        "updateDate": "345435"
    },
    {
        "id": "asdsa",
        "label": "hsajdhskaj",
        "ptoyectType": "asdasd",
        "active": "asdasdsa",
        "updateDate": "asd"
    }
];

console.log(
    result.map((item)=>item.id)
);

var corsTesterLib = require('cors-tester');
//Full return values, with errors and responses
corsTesterLib.makeCorsRequest('http://localhost:3040', function(returnValue) {
  console.log(returnValue);
});