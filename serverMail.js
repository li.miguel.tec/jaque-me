var nodemailer = require('nodemailer');
var aux = `
div(style='\
width: 552px;\
color: #373737;\
border-radius: 8px;\
border: 1px solid #eee;\
')
  div(style='padding: 36px 63px 0; border-bottom: 1px solid #f2f2f2')
    div(style='border-bottom: 1px solid #f2f2f2; padding-bottom: 39px')
      img(style='width: 73px; height: 95px; margin-bottom: 33px;', src='https://firebasestorage.googleapis.com/v0/b/ggi-journey-development.appspot.com/o/emailTemplates%2Flogo-miyana%402x.png?alt=media&token=f3804ee7-c24c-404b-b6ff-2aa85d3b3669')
      div(style='font-size: 24px; font-weight: bold') Hola Daniela
      div(style='\
      font-size: 16px;\
      margin-top: 14px;\
      line-height: 25px;\
      width: 350px;\
      ')
        | Hoy es la visita virtual con 
        b Miguel Peralta
      div(style='\
      text-align: center;\
      margin-top: 20px;\
      border-bottom: 1px solid #f2f2f2;\
      padding-bottom: 39px;\
      ')
        button(style='\
        background: #e91e2d;\
        color: #fff;\
        font-size: 14px;\
        border: none;\
        width: 192px;\
        height: 48px;\
        border-radius: 4px;\
        ')
          | Ver detalle del cliente
      div(style='font-size: 14px; margin-top: 24px')
        | Estos son los datos de tu visita
      table(border='0', cellpadding='”0”')
        tr
          td
            img(src='https://firebasestorage.googleapis.com/v0/b/ggi-journey-development.appspot.com/o/emailTemplates%2Ficon-calendar%402x.png?alt=media&token=a8a68680-c774-4e6f-b83b-b59596cfac27')
          td(style='padding: 10px 0 0 12px')
            div(style='font-size: 12px; font-weight: bold') Fecha y hora
            div(style='font-size: 12px; margin-top: 4px')
              | 28 de octubre del 2020 12:00hrs
        tr
          td
            img(src='https://firebasestorage.googleapis.com/v0/b/ggi-journey-development.appspot.com/o/emailTemplates%2Ficon-building%402x.png?alt=media&token=237d3360-e1c7-481d-afc4-4ba32296c45a')
          td(style='padding: 10px 0 0 12px')
            div(style='font-size: 12px; font-weight: bold') Interesado en
            div(style='font-size: 12px; margin-top: 4px')
              | Departamento Tipo M
      div(style='text-align: center; margin-top: 33px')
        button(style='\
        background: #e91e2d;\
        color: #fff;\
        font-size: 14px;\
        border: none;\
        width: 192px;\
        height: 48px;\
        border-radius: 4px;\
        ')
          | Link de la visita
      div(style='text-align: center; margin-top: 26px')
        button(style='\
        background: none;\
        color: #1f4489;\
        font-size: 14px;\
        border: none;\
        font-weight: bold;\
        cursor: pointer;\
        cursor: pointer;\
        ')
          | Agregar a calendario
    div(style='\
    border-bottom: 1px solid #f2f2f2;\
    padding: 25px 0 29px;\
    text-align: center;\
    ')
      div(style='font-size: 12px; font-weight: bold')
        | A&uacute;n est&aacute;s a tiempo de invitar a alguien m&aacute;s a la visita
      button(style='\
      background: none;\
      color: #e91e2d;\
      border: 2px solid #e91e2d;\
      font-size: 14px;\
      border-radius: 4px;\
      font-weight: bold;\
      cursor: pointer;\
      width: 194px;\
      height: 50px;\
      margin-top: 20px;\
      ')
        | Agregar invitados
  div(style='width: 100%; height: 86px; text-align: center; padding-top: 21px')
    div
      img(style='margin-right: 43px', src='https://firebasestorage.googleapis.com/v0/b/ggi-journey-development.appspot.com/o/emailTemplates%2Flogo-GGI%402x.png?alt=media&token=b5dd6ac9-e46d-46f0-acb9-6bba60cd80bf')
      img(style='margin: 15px 0', src='https://firebasestorage.googleapis.com/v0/b/ggi-journey-development.appspot.com/o/emailTemplates%2Flogo-GGIJourney.png?alt=media&token=16a90e68-89a4-48c2-8532-da1b0176453b')

`;

var smtpConfig = {
    host: 'smtp.mailgun.org',
    port: 587,
    secure: false, // use SSL
    priority: 'high',
    auth: {
        user: 'postmaster@ggi-journey.jaque.dev',
        pass: '9df3fb46a1f0372d88e675a953170b8f-2fbe671d-67b611c7'
    },
    tls: {
        rejectUnauthorized: false
    },
    ignoreTLS: false
};
var transporter = nodemailer.createTransport(smtpConfig);
var mailData = {
    from: 'postmaster@ggi-journey.jaque.dev',
    to: 'miguel.tec@grupojaque.com',

    subject: 'test 123',
    html: 'Hola mike'
}
transporter.sendMail(mailData, function(error, info){
    if (error){
        console.info( 'Error', error );
        var ObjDevuelve = {lError:true, cMsg:'No se pudo enviar el correo electrónico. Favor de verificar'};
        return reject(ObjDevuelve);
    }else{
        console.info( 'Ok', mailData.to );
        var ObjDevuelve = {lError:false, cMsg:'Mensaje enviado a: ' + mailData.to};
        return resolve(ObjDevuelve);
    }
});